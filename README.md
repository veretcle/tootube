# What is it?

This program takes the very last or a specified video published on a PeerTube instance and pushes it to a corresponding YouTube channel.

# What the status of this?

This is an early prototype not really suited for production purposes for now:
* it still relies way too much on pre-determined value to upload the video to YouTube
* it cannot determine the recording date (believe me, I tried!)

So consider this a work in progress that will slowly get better with time.

# What does it do exactly?

* it retrieves the latest (or a specified) PeerTube video download URL from an instance
* if you register the app into your PeerTube instance (optional), you can retrieve the latest video source instead of the highest quality encoded video
* it creates a resumable upload into the target YouTube account
* it downloads/uploads the latest PeerTube video to YouTube without using a cache (stream-to-stream)

# What doesn’t it do exactly?

* it doesn’t retrieve ALL the original PeerTube video properties like licences, languages, categories, etc… again: early prototype

# Howtos
## General usage

Once you fill your `tootube.toml` config file (see below), you can run `tootube` like this:

```bash
tootube --config tootube.toml --playlist playlist_1 "Things You Might Like"
```

You can turn on debug using env var `RUST_LOG`.

## Obtain Authorization Token from Google

The complicated part:
* create an OAuth2.0 application with authorization for Youtube DATA Api v3 Upload and Youtube DATA Api v3 (generally referenced as `../auth/youtube.upload` and `../auth/youtube`)
* create a OAuth2.0 client with Desktop client

You’ll need:
* the `client_id` from your OAuth2.0 client
* the `client_secret` from you OAuth2.0 client

Create your `tootube.toml` config file:

```toml
[peertube]
base_url="https://p.nintendojo.fr"
# optional
# allows you to delete the original video source file once it’s uploaded to YouTube
delete_video_source_after_transfer=true # this option is only available if you have Administrator privileges
# optional
# everything below is given by the register command with --peertube option
[peertube.oauth2]
client_id="<YOUR CLIENT_ID>"
client_secret="<YOUR CLIENT_SECRET>"
refresh_token="<YOUR CLIENT TOKEN>"

[youtube]
notify_subscribers_on_shorts=false # will you notify subscribers for shorts?
# optional
# allows you to notify subscribers when transferring shorts, defaults to false

[youtube.oauth2]
refresh_token="" # leave empty for now
client_id="<YOUR CLIENT_ID>"
client_secret="<YOUR CLIENT_SECRET>"
```

Then run:

```bash
tootube register --youtube --config <PATH TO YOUR TOOTUBE.TOML FILE>
```

You’ll be then prompted with all the necessary information to register `tootube`. You’ll end with a `refresh_token` that you will be written back to the config file.

If you wish to register `tootube` on PeerTube, you can do so by using:

```bash
tootube register --peertube --config <PATH TO YOUR TOOTUBE.TOML FILE>
```

It will require your username/password (beware that 2FA is not supported for this feature as of now) and generate a first `refresh_token` that will be written back to the config file.
