use clap::{Arg, Command};
use tootube::*;

const DEFAULT_CONFIG_PATH: &str = "/usr/local/etc/tootube.toml";

fn main() {
    let matches = Command::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .about("A simple PeerTube to YouTube converter")
        .arg(
            Arg::new("config")
                .short('c')
                .global(true)
                .long("config")
                .value_name("CONFIG_FILE")
                .help("TOML config file for tootube")
                .num_args(1)
                .default_value(DEFAULT_CONFIG_PATH)
                .display_order(1),
        )
        .arg(
            Arg::new("playlists")
                .short('p')
                .long("playlist")
                .value_name("PLAYLIST")
                .help("List of playlists to add the video to")
                .num_args(0..)
                .display_order(2),
        )
        .arg(
            Arg::new("id")
                .short('i')
                .long("id")
                .value_name("ID")
                .help("Specify the PeerTube Video ID")
                .num_args(0..=1)
                .display_order(3),
        )
        .arg(
            Arg::new("vice")
                .long("vice")
                .aliases(["chybrare", "coquinou"])
                .action(clap::ArgAction::SetTrue)
                .display_order(4),
        )
        .subcommand(
            Command::new("register")
                .version(env!("CARGO_PKG_VERSION"))
                .about("Command to register to YouTube or PeerTube OAuth2.0")
                .arg(
                    Arg::new("youtube")
                        .long("youtube")
                        .short('y')
                        .required(true)
                        .conflicts_with("peertube")
                        .action(clap::ArgAction::SetTrue),
                )
                .arg(
                    Arg::new("peertube")
                        .long("peertube")
                        .short('p')
                        .required(true)
                        .action(clap::ArgAction::SetTrue),
                ),
        )
        .get_matches();

    if let Some(("register", sub_m)) = matches.subcommand() {
        let mut config = Config::new(sub_m.get_one::<String>("config").unwrap());

        if sub_m.get_flag("youtube") {
            let yt_refresh_token = register_youtube(&config.youtube.oauth2)
                .unwrap_or_else(|e| panic!("Cannot register to YouTube API: {}", e));

            config.youtube.oauth2.refresh_token = yt_refresh_token;
        }

        if sub_m.get_flag("peertube") {
            let pt_oauth2 = register_peertube(&config.peertube)
                .unwrap_or_else(|e| panic!("Cannot register to PeerTube API: {}", e));

            config.peertube.oauth2 = Some(pt_oauth2);
        }

        config
            .dump(sub_m.get_one::<String>("config").unwrap())
            .unwrap_or_else(|e| panic!("Cannot write back to Tootube Config file: {}", e));

        return;
    }

    let mut config = Config::new(matches.get_one::<String>("config").unwrap());

    if matches.get_flag("vice") {
        config.tootube.progress_bar = "{msg}\n[{elapsed_precise}] 8{wide_bar:.magenta}ᗺ {bytes}/{total_bytes} ({bytes_per_sec}, {eta})".to_string();
        config.tootube.progress_chars = "=D ".to_string();
    }

    let playlists: Vec<String> = matches
        .get_many::<String>("playlists")
        .unwrap_or_default()
        .map(|v| v.to_string())
        .collect();

    let pt_video_id = matches.get_one::<String>("id").map(|s| s.as_str());

    env_logger::init();

    // runs the main program logic & retrieves the updated PeerTube refresh_token
    if let Some(x) = run(&config, playlists, pt_video_id) {
        config.peertube.oauth2 = config.peertube.oauth2.map(|y| PeertubeConfigOauth2 {
            client_id: y.client_id,
            client_secret: y.client_secret,
            refresh_token: x,
        });

        let _ = config.dump(matches.get_one::<String>("config").unwrap());
    };
}
