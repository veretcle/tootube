use log::debug;

mod error;

mod config;
pub use config::Config;
pub use config::PeertubeConfigOauth2;

mod peertube;
pub use peertube::register as register_peertube;
use peertube::{get_playlists_to_be_added_to, PeerTube};

mod youtube;
pub use youtube::register as register_youtube;
use youtube::YouTube;

/// This is where the magic happens
/// This takes the main options from config & command line args
/// and sends back the updated PeerTube refresh_token if any
#[tokio::main]
pub async fn run(config: &Config, pl: Vec<String>, pt_video_id: Option<&str>) -> Option<String> {
    // Create PeerTube struct
    let peertube = match &config.peertube.oauth2 {
        Some(s) => PeerTube::new(&config.peertube.base_url)
            .with_client(s)
            .await
            .unwrap_or_else(|e| panic!("Cannot instantiate PeerTube struct: {}", e)),
        None => PeerTube::new(&config.peertube.base_url),
    };
    // Get the latest video object or the targeted pt_video_id
    let latest_vid = match pt_video_id {
        None => peertube.get_latest_video().await.unwrap_or_else(|e| {
            panic!("Cannot retrieve the latest video, something must have gone terribly wrong: {e}")
        }),
        Some(v) => peertube.get_video_detail(v).await.unwrap_or_else(|e| {
            panic!(
                "Cannot retrieve the specified video, something must have gone terribly wrong: {e}"
            )
        }),
    };

    // We have a refresh_token, try to use it
    let source_url = match &config.peertube.oauth2 {
        Some(_) => peertube
            .get_original_video_source(&latest_vid.uuid)
            .await
            .ok(),
        None => None,
    };

    // Whatever happens, collect the highest quality possible
    let high_quality_url = latest_vid.streaming_playlists.as_ref().unwrap()[0]
        .files
        .iter()
        .max()
        .unwrap()
        .file_download_url
        .clone();

    // dl_url corresponds to source url if available, best quality if not
    let dl_url = match source_url {
        Some(s) => s,
        None => high_quality_url,
    };

    debug!("PT download URL: {}", &dl_url);

    let youtube = YouTube::new(
        &config.youtube.oauth2.client_id,
        &config.youtube.oauth2.client_secret,
        &config.youtube.oauth2.refresh_token,
    )
    .await
    .unwrap_or_else(|e| panic!("Cannot instantiate YouTube struct: {}", e));

    // Do not notify when notify_subscribers_on_shorts = False and latest_id is a short
    debug!(
        "Will user get notified? {}",
        !(!config.youtube.notify_subscribers_on_shorts & latest_vid.is_short())
    );

    let resumable_upload_url = youtube
        .create_resumable_upload(
            &latest_vid,
            !(!config.youtube.notify_subscribers_on_shorts & latest_vid.is_short()),
        )
        .await
        .unwrap_or_else(|e| panic!("Cannot retrieve the upload’s resumable id: {e}"));
    debug!("YT upload URL: {}", &resumable_upload_url);

    let yt_video_id = youtube
        .now_kiss(&dl_url, &resumable_upload_url, &config.tootube)
        .await
        .unwrap_or_else(|e| panic!("Cannot resume upload!: {e}"));
    debug!("YT video ID: {}", &yt_video_id);

    youtube
        .set_thumbnail(
            &yt_video_id,
            &format!("{}{}", &config.peertube.base_url, latest_vid.preview_path),
        )
        .await
        .unwrap_or_else(|e| panic!("Cannot upload the thumbnail: {}", e));

    if !pl.is_empty() {
        youtube
            .add_video_to_playlists(&yt_video_id, &pl)
            .await
            .unwrap_or_else(|e| panic!("Cannot add video to playlist(s): {e}"));
    }

    // delete the source video if requested (it won’t be used anymore)
    if config.peertube.delete_video_source_after_transfer {
        peertube
            .delete_original_video_source(&latest_vid.uuid)
            .await
            .unwrap_or_else(|e| panic!("Cannot delete source video: {e}"));

        debug!("Original Video {} has been deleted", &latest_vid.uuid);
    }

    // Updates the playlist of PeerTube if necessary
    if config.peertube.oauth2.is_some() && !pl.is_empty() {
        debug!("Updating playlists on PeerTube");

        if let Ok(pl_to_add_to) =
            get_playlists_to_be_added_to(&peertube, &latest_vid.uuid, &pl, latest_vid.channel.id)
                .await
        {
            for p in pl_to_add_to {
                let _ = peertube.add_video_to_playlist(&latest_vid.uuid, &p).await;
            }
        }
    }

    peertube.refresh_token
}
