use std::{
    boxed::Box,
    convert::From,
    error::Error,
    fmt::{Display, Formatter, Result},
};

#[derive(Debug)]
pub struct TootubeError {
    details: String,
}

impl TootubeError {
    pub fn new(msg: &str) -> TootubeError {
        TootubeError {
            details: msg.to_string(),
        }
    }
}

impl Error for TootubeError {}

impl Display for TootubeError {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "{}", self.details)
    }
}

impl From<Box<dyn Error>> for TootubeError {
    fn from(error: Box<dyn Error>) -> Self {
        TootubeError::new(&format!("Error in a subset crate: {error}"))
    }
}
